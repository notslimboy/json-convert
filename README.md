# Bubble Chat Text From JSON

## 4210181030 Raka Arya Pratama 

### Implemented JSON Text from JSON File to Unity Text with Newtonsoft.JSON Plugins

#### Here is the demonstration gif : 

![demonstration](/JsonConvert.gif)*JSON Text will load on application start*


#### Here is the flow of the program : 
![flow](/Documentation/Flow.png)

#### First we need to setup the plugins. We will use Newtonsoft.JSON for Unity. Add this code to Unity Manifest Package.
![Manifest](/Documentation/setup unity manifest.png)

We need write that code because we will get the package from NPM modules and add dependecies to make the package recognize by Unity.
#### Add this code on namespaces.
### `using Newtonsoft.Json;`
this namespaces will use the Newtonsoft API.
#### Create custom class on TextFromJSON.cs  
```
public class Person
{
    public string name;
    public string email;
    public Person (string name, string email)
    {
        this.name = name;
        this.email = email;
    }
}
```
This class will contain data struct from JSON. If wanna retrieving name and email, then we need a custom class that contain name and email.

#### Get the JSON from URL 
``` 
 private void Awake()
    {
        WWW req = new WWW(linkJSON);
        StartCoroutine(ProcessRequestJSON(req));
    }

```
This code will execute retrieving JSON file from certain URL.

#### Get JSON File field 
``` 
 private string GetPersonName(string json)
    {
        List<Person> PersonInfo = JsonConvert.DeserializeObject<List<Person>>(json);
        return PersonInfo[index].name.ToString();
    }

    private string GetPersonEmail(string json)
    {
        List<Person> PersonInfo = JsonConvert.DeserializeObject<List<Person>>(json);
        return PersonInfo[index].email.ToString();
    }
```
Deserialize JSON struct data to string





